<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="index.css">
</head>
    <body>
        <ul class="navbar">
                <b class="leftside"><a>PORTFOLIO | Abdur Rahim C. Jumlani</a></b>
        </ul>
        
        <div id="mainDiv">
                <div class="sectionDiv">
                    
        
                    <div class="detailDiv">
        
                        <div class="introParagraph">
                            <h1><span> Abdur Rahim C. Jumlani</span></h1>
                            <p class="statName">IT Professional</p>
                            <p>
                                Hi there my name is Rahim! I am a IT professional graduated from the University
                                of Southeastern Philippines. Originally from Bacolod City but I have
                                studied and completed my profession here in Davao.
                            </p>
                        </div>
        
                        <div class="infoDiv">
                            <div class="infoDetails">
                                <h4>PERSONAL INFORMATION</h4>
                                <ul>
                                    <hr />
                                    <p><b>Age:</b> 21<br>
                                    <b>Contact:</b> (+63) 932 429 1727<br>
                                    <b>Gender:</b> Male<br>
                                    <b>Email Address:</b> rahimjumlani@gmail.com<br>
                                    <b>Physical Address:</b> Ultra Homes subd., Matina Aplaya, Davao City<br>
                                    <b>Birthdate:</b> May 26, 1997</p>
                                </ul>
                            </div>
                        </div>


                    </div>

                    <div class="pictureDiv">
                            <img title="Profile Picture" src="ProfilePic.jpg">
                    </div>
                    
                </div>
        
                <div class="sectionDiv">
                    <h2 title="Additional Information" >| ADDITIONAL INFORMATION |</h2>
                    <div class="skillDiv">
                                <div class="infoDiv">
                                    <div class="infoDetails">
                                        <h4>EDUCATIONAL BACKGROUND</h4>
                                        <ul>
                                            <hr />
                                            <p><b>Tertiary:</b> University of Southeastern Philippines<br>
                                            <b>Secondary:</b> Mount Olives Christian Academy<br>
                                            <b>Primary:</b> University of St. La Salle<br>
                                            <b>Additional Education:</b> TESDA Computer Hardware Servicing NCII<br>
                                            </p>
                                        </ul>
                                        <br>
                                        <h4>WORK EXPERIENCE</h4>
                                        <ul>
                                            <hr />
                                            <p><b>SSI Davao</b> - Market Research Interviewer<br>
                                            </p>
                                        </ul>
                                        <br>
                                        <h4>SPORTS</h4>
                                        <ul>
                                            <hr />
                                            <p>Football/Soccer - Defender<br>
                                            </p>
                                        </ul>
                                    </div>
                                </div>

                                <div class="infoDetails">
                                    <h4>SKILLS</h4>
                                    <hr />
                                        <p>Speaks English fluently<br>
                                           Computer literate<br>
                                           Good Communication<br>
                                           Ability to Work Under Pressure<br>
                                           Decision Making<br>
                                           Time Management<br>
                                           Leadership <em>(President - SITS)</em><br>
                                           Adaptability<br>
                                           Conflict Resolution<br>
                                           Creativity<br>
                                        </p>
                            </div>
                        
                    </div>
                </div>
            </div>
        
        <hr />
    </body>
</html>